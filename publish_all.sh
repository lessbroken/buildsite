#!/bin/bash
mydir=$(dirname $0)
source $mydir/../config

shopt -s extglob
rsync -rv $mydir/../output/* $mydir/../output/static $mydir/../content $desthost:$destdir
