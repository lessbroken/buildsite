#!/bin/bash

mydir=$(dirname $0)
outdir=$mydir/../output/static
indir=$mydir/../static
source $mydir/functions.sh

mkdir -p $outdir
for infile in $indir/*; do
    printf "%30s\n" "$(resolve $infile)"
    cp $infile $outdir
done

