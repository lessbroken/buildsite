import Text.Pandoc
import Text.Pandoc.JSON

setLinkAttr :: Inline -> Inline
setLinkAttr (Link (linkID, linkClass, linkAttrs) inlines target) =
    Link (linkID, linkClass, (linkAttrs ++ [("target", "_blank"), ("rel", "noopener noreferrer")])) inlines target
setLinkAttr x = x

main :: IO ()
main = toJSONFilter setLinkAttr
