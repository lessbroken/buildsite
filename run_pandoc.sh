#!/bin/bash

infile=$1
outfile=$2
template=${3:-default}

possible_template=$(grep -E "^template: [a-z0-9]+" $infile)
if test -n "$possible_template"; then template=$(echo $possible_template | cut -d' ' -f2); fi


mydir=$(dirname $0)

pandoc \
    --from=markdown \
    --to=html \
    --template=templates/$template \
    --standalone \
    --toc \
    --filter $mydir/fix_link_target.hs \
    $infile \
    -o $outfile
