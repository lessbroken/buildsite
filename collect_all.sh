#!/bin/bash

mydir=$(dirname $0)
source $mydir/functions.sh

for infile in $(find "$mydir/../content/" -name '*.md'); do
    outfile=$(echo $infile | sed -e 's/md$/html/' -e 's/\/content\//\/output\//')
    mkdir -p $(dirname $outfile)
    printf "%30s -> %30s\n" "$(resolve $infile)" "$(resolve $outfile)"
    $mydir/run_pandoc.sh $infile $outfile
done
