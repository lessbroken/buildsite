mydir=$(dirname $0)

resolve() {
    realpath --relative-to $mydir/.. $1
}
