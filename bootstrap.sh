#!/bin/bash
mydir=$(dirname $0)
base=$mydir/..
target=$1

mkdir -p $target/{content,output}
cp -r $base/{Makefile,config,templates,static,.gitignore} $target

pushd $target
git init
git submodule init
git submodule add git://lessbroken.org/pandoc-build-scripts.git build
git add .
git commit -m "initial commit"
popd

echo "Done; project created in $target with duplicate templates and static files."
echo "Don't forget to edit $target/config and add appropriate values for deployment."
